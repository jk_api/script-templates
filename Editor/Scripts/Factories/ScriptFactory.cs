﻿namespace Game.ScriptTemplates
{
    using System.IO;
    using UnityEditor;

    public static class ScriptFactory
    {
        public static void CreateScriptFromTemplateAsset(string templatePath, string templateAssetName)
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(templatePath, $"New{templateAssetName}.cs");
        }
        public static void CreateScriptFromTemplateAssetFixName(string templatePath, string templateAssetName, bool useNameFolder)
        {
            string currentPath = AssetDatabase.GetAssetPath(Selection.activeObject);
            string nameFolder = useNameFolder ? Path.GetFileNameWithoutExtension(currentPath) : "";
            string name = $"{nameFolder}{templateAssetName}.cs".Replace(" ", "_");

            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(templatePath, name);
        }
    }
}