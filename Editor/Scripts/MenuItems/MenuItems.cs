﻿namespace Game.ScriptTemplates
{
    using System.IO;
    using UnityEditor.Compilation;
    using UnityEditor;
    using UnityEngine;

    public class TemplateMenuItems
    {
        const int Priority = 51;
        #region Architecture
        const string Architecture = "Architecture";
        const string Systems = "System";
        const string Commands = "Command";
        const string Events = "Event";
        const string Models = "Model";
        const string PathArchitecture = "Assets/Create/Architecture/";
        [MenuItem(PathArchitecture + Architecture, priority = Priority)]
        static void MenuArchitecture()
        {
            string pathTemplete = GetTemplatePath(Architecture);
            if (!string.IsNullOrEmpty(pathTemplete))
            {
                CreateArchitecture(pathTemplete, Architecture);
            }
        }

        [MenuItem(PathArchitecture + Systems, priority = Priority)]
        static void MenuSystem()
        {
            string pathTemplete = GetTemplatePath(Architecture);
            if (string.IsNullOrEmpty(pathTemplete)) return;
            var dir = Directory.GetParent(AssetDatabase.GetAssetPath(Selection.activeObject));

            string path = Path.Combine(Directory.GetParent(pathTemplete).FullName, $"{nameof(Systems)}.txt");

            CreateScriptFromTemplete(path, $"{dir.Name}System", false);

        }
        [MenuItem(PathArchitecture + Commands, priority = Priority)]
        static void MenuCommand()
        {
            string pathTemplete = GetTemplatePath(Architecture);
            if (string.IsNullOrEmpty(pathTemplete)) return;
            var dir = Directory.GetParent(AssetDatabase.GetAssetPath(Selection.activeObject));

            string path = Path.Combine(Directory.GetParent(pathTemplete).FullName, $"{nameof(Commands)}.txt");

            CreateScriptFromTemplete(path, $"{dir.Name}Command", false);

        }
        [MenuItem(PathArchitecture + Models, priority = Priority)]
        static void MenuModel()
        {
            string pathTemplete = GetTemplatePath(Architecture);
            if (string.IsNullOrEmpty(pathTemplete)) return;

            string path = Path.Combine(Directory.GetParent(pathTemplete).FullName, $"{nameof(Models)}.txt");
            ScriptFactory.CreateScriptFromTemplateAsset(path, Models);
        }
        [MenuItem(PathArchitecture + Events, priority = Priority)]
        static void MenuEvent()
        {
            string pathTemplete = GetTemplatePath(Architecture);
            if (string.IsNullOrEmpty(pathTemplete)) return;
            var dir = Directory.GetParent(AssetDatabase.GetAssetPath(Selection.activeObject));
            string path = Path.Combine(Directory.GetParent(pathTemplete).FullName, $"{nameof(Events)}.txt");
            CreateScriptFromTemplete(path, $"{dir.Name}Events", false);
            ProjectWindowUtil.ShowCreatedAsset(Selection.activeObject);
        }
        public static string GetTemplatePath(string Name)
        {
            string[] paths = Directory.GetDirectories(Application.dataPath, "*", SearchOption.TopDirectoryOnly);
            foreach (string path in paths)
            {
                string[] rs = Directory.GetFiles(path, $"{Name}.txt", SearchOption.AllDirectories);

                if (rs != null && rs.Length > 0)
                {
                    return rs[0];
                }
            }
            return "";
        }
        static void CreateArchitecture(string path, string name)
        {
            DirectoryInfo dir = Directory.GetParent(path);
            string currentPath = AssetDatabase.GetAssetPath(Selection.activeObject);
            foreach (var item in dir.GetFiles("*.txt"))
            {
                string Name = Path.GetFileNameWithoutExtension(item.Name);
                if (Name == Architecture) continue;
                Directory.CreateDirectory(Path.Combine(currentPath, Name));
            }
            CreateScriptFromTemplete(path, name);
            AssetDatabase.Refresh();
        }
        static void CreateScriptFromTemplete(string path, string name, bool useNameFolder = true)
        {
            ScriptFactory.CreateScriptFromTemplateAssetFixName(path, name, useNameFolder);
        }
        #endregion

        #region State machine
        const string PathStateMachine = "Assets/Create/StateMachine/";
        const string State = "State";
        const string StateMachine = "StateMachine";
        [MenuItem(PathStateMachine + StateMachine, priority = Priority)]
        static void MenuStateMachine()
        {
            string pathTemplete = GetTemplatePath(StateMachine);
            Debug.Log(pathTemplete);
            if (!string.IsNullOrEmpty(pathTemplete))
            {
                CreateStateMachine(pathTemplete, StateMachine);
            }
        }
        static void CreateStateMachine(string path, string name)
        {
            DirectoryInfo dir = Directory.GetParent(path);
            string currentPath = AssetDatabase.GetAssetPath(Selection.activeObject);
            foreach (var item in dir.GetFiles("*.txt"))
            {
                string Name = Path.GetFileNameWithoutExtension(item.Name);
                if (Name == name) continue;
                Directory.CreateDirectory(Path.Combine(currentPath, Name));
            }
            CreateScriptFromTemplete(path, name);
            AssetDatabase.Refresh();
        }
        [MenuItem(PathStateMachine + State, priority = Priority)]
        static void MenuState()
        {
            string pathTemplete = GetTemplatePath(StateMachine);
            if (string.IsNullOrEmpty(pathTemplete)) return;
            var dir = Directory.GetParent(AssetDatabase.GetAssetPath(Selection.activeObject));

            string path = Path.Combine(Directory.GetParent(pathTemplete).FullName, $"{nameof(State)}.txt");

            CreateScriptFromTemplete(path, $"{dir.Name}State", false);

        }
        #endregion
    }
}